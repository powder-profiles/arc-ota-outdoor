set -ex
BINDIR=`dirname $0`
source $BINDIR/common.sh

SDKMANAGER_PKG=$NVIDIADIR/sdkmanager_2.1.0-11698_amd64.deb
GNB_SDKM_RESPONSE_FILE=$CFGDIR/nvsdkm/sdkm_responsefile_arc_cudu.ini

if [ -f $SRCDIR/arc-setup-complete ]; then
    echo "setup already ran; not running again"
    exit 0
fi

### do arc-ota deploy
sudo apt-get -q update
sudo apt-get -q -y install $SDKMANAGER_PKG
# sudo sdkmanager --cli --auto --response-file $GNB_SDKM_RESPONSE_FILE

touch $SRCDIR/arc-setup-complete
