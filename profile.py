#!/usr/bin/env python

import os

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as ig
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab as emulab
import geni.rspec.emulab.lanext as lanext


tourDescription = """
### ARC-OTA Outdoor Profile Example

This profile automatically installs the NVidia SDK Manager on the CUDU node,
and then runs the installation process for ARC from there (under development).

Once complete, this profile will stand up an E2E ARC-OTA instance, with
accelerated 5G and CU+DU running on specialized compute nodes containing
NVidia acclerator technologies (Grace Hopper and/or commodity PC servers
with A100X converged accelerators).

Currently, the profile deploys the Open5GS Core + srsRAN CU/DU + Benetel n78 RU
for illustrative purposes.

"""

tourInstructions = """
### ARC-OTA Outdoor Instructions
"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
DEFAULT_SRSRAN_HASH = "a15950301c5f3a1a166b79bb6c9ee901a4e8c2dd"
OPEN5GS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-open5gs.sh")
SRSRAN_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-srsran.sh")
DEFAULT_NVIDIA_RBS_URN = "urn:publicid:IDN+emulab.net:powderteam+ltdataset+nvidia-sdk"
DEFAULT_NVIDIA_RBS_MP = "/opt/nvidia"

pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]
pc.defineParameter(
    name="sdr_nodetype",
    description="Type of compute node paired with the SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[1],
    legalValues=node_types
)

pc.defineParameter(
    name="cn_nodetype",
    description="Type of compute node to use for CN node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

pc.defineParameter(
    name="cudu_compute_id",
    description="Component ID for compute node connected to RU",
    typ=portal.ParameterType.STRING,
    defaultValue="pc01-meb",
)

pc.defineParameter(
    name="sdr_compute_image",
    description="Image to use for compute connected to SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="srsran_commit_hash",
    description="Commit hash for srsRAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="sdr_compute_image",
    description="Image to use for compute connected to SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="rbs_urn",
    description="URN of remote blockstore containing NVidia SDK components",
    typ=portal.ParameterType.STRING,
    defaultValue=DEFAULT_NVIDIA_RBS_URN,
    advanced=True
)

pc.defineParameter(
    name="rbs_mp",
    description="Mountpoint on node(s) for NVidia remote blockstore",
    typ=portal.ParameterType.STRING,
    defaultValue=DEFAULT_NVIDIA_RBS_MP,
    advanced=True
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

### Currently deploying Open5GS and srsRAN on two seperate compute nodes and
### connecting the srsRAN node (CU/DU) to a Benetel O-RU with both synced to
### the powder PTP grandmaster and vlans configured for the OFH interface.
###
### In the future, when we have ARC-OTA hardware, the "cn5G" node can disappear,
### the "cudu" node will become, e.g., GH200 nodes, where the NVIDIA SDK manager
### can be used to deploy/configure the necessary software components. The "bru"
### node will become a Foxconn RPQN-780xE node.

### Open5GS deployment left for illustrative purposes
node_name = "cn5g"
cn_node = request.RawPC(node_name)
cn_node.component_manager_id = COMP_MANAGER_ID
cn_node.hardware_type = params.cn_nodetype
cn_node.disk_image = UBUNTU_IMG
cn_if = cn_node.addInterface("{}-if".format(node_name))
cn_if.addAddress(pg.IPv4Address("192.168.1.1", "255.255.255.0"))
cn_link = request.Link("{}-link".format(node_name))
#cn_link.setNoBandwidthShaping()
cn_link.best_effort = True
cn_link.vlan_tagging = True
cn_link.link_multiplexing = True
cn_link.addInterface(cn_if)
cn_node.addService(pg.Execute(shell="bash", command=OPEN5GS_DEPLOY_SCRIPT))

node_name = "cudu"
cudu = request.RawPC(node_name)
cudu.component_manager_id = COMP_MANAGER_ID
cudu.component_id = params.cudu_compute_id
cudu.disk_image = UBUNTU_IMG
cudu_cn_if = cudu.addInterface("{}-cn-if".format(node_name))
cudu_cn_if.component_id = "eth6"
cudu_cn_if.addAddress(pg.IPv4Address("192.168.1.2", "255.255.255.0"))
cn_link.addInterface(cudu_cn_if)

dubrumgmt = cudu.addInterface("{}brumgmt".format(node_name))
dubrumgmt.component_id = "eth4"
dubrumgmt.addAddress(pg.IPv4Address("10.13.1.1", "255.255.255.0"))
dubrumgmt.PTP()
dubruofh = cudu.addInterface("{}bruofh".format(node_name))
dubruofh.component_id = "eth4"

cudu.addService(pg.Execute(shell="bash", command="sudo /local/repository/bin/setup-ptp.sh"))

### Attach remote blockstore containing NVidia SDK pieces.
cudubsintf = cudu.addInterface("ifbs1")
bsnode = request.RemoteBlockstore("bsnode1", params.rbs_mp)
bsintf = bsnode.interface
bsnode.dataset = params.rbs_urn
bsnode.rwclone = True
#
bslink = request.Link("bslink-1")
bslink.addInterface(cudubsintf)
bslink.addInterface(bsintf)
bslink.best_effort = True
bslink.vlan_tagging = True
bslink.link_multiplexing = True

### SRS deployment left for illustrative purposes
if params.srsran_commit_hash:
    srsran_hash = params.srsran_commit_hash
else:
    srsran_hash = DEFAULT_SRSRAN_HASH
cmd = "{} '{}'".format(SRSRAN_DEPLOY_SCRIPT, srsran_hash)
cudu.addService(pg.Execute(shell="bash", command=cmd))
cudu.startVNC()

### ARC-OTA deployment example
cudu.addService(pg.Execute(shell="bash", command="/local/repository/bin/deploy-arc-ota.sh"))

### Benetel RU for srsRAN CU/DU example (PTP + OFH VLAN config)
node_name = "bru"
bru = request.RawPC(node_name)
bru.component_manager_id = COMP_MANAGER_ID
bru.component_id = "bru-650-4"
brudumgmt = bru.addInterface("{}dumgmt".format(node_name))
brudumgmt.component_id = "eth0"
brudumgmt.addAddress(pg.IPv4Address("10.13.1.2", "255.255.255.0"))
brudumgmt.PTP()
brudumgmt.SyncE()
bruduofh = bru.addInterface("{}duofh".format(node_name))
bruduofh.component_id = "eth0"
dubruu = request.Link("dubruu", members=[dubrumgmt, brudumgmt])
dubruu.DualModeTrunking()
dubrut = request.Link("dubrut", members=[dubruofh, bruduofh])
dubrut.setVlanTag(24)
dubrut.DualModeTrunking(dubruu)

tour = ig.Tour()
tour.Description(ig.Tour.MARKDOWN, tourDescription)
tour.Instructions(ig.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
