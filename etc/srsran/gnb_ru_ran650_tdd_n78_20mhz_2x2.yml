# This example configuration outlines how to configure the srsRAN Project CU/DU to use an O-RU and split 7.2. This is specifically for use
# with the Benetel R550 RU. This config will create a single TDD MIMO cell transmitting in band 78, with 100 MHz bandwidth and 30 kHz sub-carrier-spacing.
# The parameters used to configure the RU are found in the `ru_ofh` sub-section. This configuration makes used of the OFH Lib from SRS to enable split 7.2.

amf:
  addr: 192.168.1.1
  bind_addr: 192.168.1.2

ru_ofh:
  t1a_max_cp_dl: 500                                              # Maximum T1a on Control-Plane for Downlink in microseconds.
  t1a_min_cp_dl: 450                                              # Minimum T1a on Control-Plane for Downlink in microseconds.
  t1a_max_cp_ul: 350                                              # Maximum T1a on Control-Plane for Uplink in microseconds.
  t1a_min_cp_ul: 290                                              # Minimum T1a on Control-Plane for Uplink in microseconds.
  t1a_max_up: 360                                                 # Maximum T1a on User-Plane in microseconds.
  t1a_min_up: 300                                                 # Minimum T1a on User-Plane in microseconds.
  ta4_max: 200                                                    # Maximum Ta4 on User-Plane in microseconds.
  ta4_min: 0                                                      # Minimum Ta4 on User-Plane in microseconds.
  is_prach_cp_enabled: true                                       # Configures if Control-Plane messages should be used to receive PRACH messages.
  compr_method_ul: bfp                                            # Uplink compression method.
  compr_bitwidth_ul: 9                                            # Uplink IQ samples bitwidth after compression.
  compr_method_dl: bfp                                            # Downlink compression method.
  compr_bitwidth_dl: 9                                            # Downlink IQ samples bitwidth after compression.
  compr_method_prach: bfp                                         # PRACH compression method.
  compr_bitwidth_prach: 9                                         # PRACH IQ samples bitwidth after compression.
  enable_ul_static_compr_hdr: false                               # Configures if the compression header is present for uplink User-Plane messages (false) or not present (true).
  enable_dl_static_compr_hdr: false                               # Configures if the compression header is present for downlink User-Plane messages (false) or not present (true).
  iq_scaling: 7.5                                                 # IQ samples scaling factor applied before compression, should be a positive value smaller than 10.
  cells:
    - network_interface: enp134s0f0                                 # Ethernet interface name used to communicate with the RU.
      ru_mac_addr: 70:b3:d5:e1:5d:13                              # RU MAC address.
      du_mac_addr: f8:f2:1e:4c:b2:b0
      vlan_tag_cp: 24                                             # VLAN tag value.
      vlan_tag_up: 24                                             # VLAN tag value.
      prach_port_id: [4, 5]                                       # PRACH eAxC port value.
      dl_port_id: [0, 1]                                          # Downlink eAxC port values.
      ul_port_id: [0, 1]                                             # Uplink eAxC port values.

cell_cfg:
  dl_arfcn: 624668                                                # (3370.02) RU set to 627332 (3409.98) ARFCN of the downlink carrier (center frequency).
  band: 78                                                        # The NR band.
  channel_bandwidth_MHz: 20                                       # Bandwith in MHz. Number of PRBs will be automatically derived.
  common_scs: 30                                                  # Subcarrier spacing in kHz used for data.
  plmn: "99999"                                                   # PLMN broadcasted by the gNB.
  tac: 1                                                          # Tracking area code (needs to match the core configuration).
  pci: 1                                                          # Physical cell ID.
  nof_antennas_dl: 2
  nof_antennas_ul: 2
  prach:
    prach_config_index: 159                                       # PRACH configuration index.
    prach_root_sequence_index: 1                                  # PRACH root sequence index.
    zero_correlation_zone: 0                                      # Zero correlation zone.
    prach_frequency_start: 2                                      # Offset in PRBs of lowest PRACH transmission occasion in frequency domain respective to PRB 0.
    preamble_trans_max: 50
    power_ramping_step_db: 4
  tdd_ul_dl_cfg:
    dl_ul_tx_period: 10                                           # Optional INT (10). Sets the TDD pattern periodicity in slots. The combination of this value and the chosen numerology must lead to a TDD periodicity of 0.5, >
    nof_dl_slots: 7                                               # Optional INT (6). Number of consecutive full Downlink slots. Supported: [0-80].
    nof_dl_symbols: 6                                             # Optional INT (0). Number of Downlink symbols at the beginning of the slot following full Downlink slots. Supported: [0-13].
    nof_ul_slots: 2                                               # Optional INT (3). Number of consecutive full Uplink slots. Supported: [0 - 80].
    nof_ul_symbols: 4
  ssb:
    ssb_block_power_dbm: -20
  pdsch:
    olla_target_bler: 0.1
  pusch:
    olla_target_bler: 0.1
    p0_nominal_with_grant: -76

expert_execution:
  threads:
    upper_phy:
      nof_dl_threads: 2
      nof_ul_threads: 2
    ofh:
      enable_dl_parallelization: true

  cell_affinities:
    -
      l1_dl_cpus: 23,21,19,17                                     # Optional TEXT. Sets the number of CPU cores assigned to L1 downlink tasks. Supported: [1 - Number of available cores].
      l1_ul_cpus: 15,7                                            # Optional TEXT. Sets the number of CPU cores assigned to L1 uplink tasks. Supported: [1 - Number of available cores].
      l2_cell_cpus: 13,5                                          # Optional TEXT. Sets the number of CPU cores assigned to L2 cells tasks. Supported: [1 - Number of available cores].
      l1_dl_pinning: mask                                         # Optional TEXT. Sets the policy used for assigning CPU cores to L1 downlink tasks.
      l1_ul_pinning: mask                                         # Optional TEXT. Sets the policy used for assigning CPU cores to L1 uplink tasks.
      l2_cell_pinning: mask                                       # Optional TEXT. Sets the policy used for assigning CPU cores to L2 cell tasks.
  affinities:
    low_priority_cpus: 37,13                                      # Optional TEXT. Sets the number of CPU cores assigned to low priority tasks. Supported: [1 - Number of available cores].
    low_priority_pinning: mask                                    # Optional TEXT. Sets the policy used for assigning CPU cores to low priority tasks.

log:
  filename: /tmp/gnb.log                                          # Path of the log file.
  all_level: warning                                                 # Logging level applied to all layers.
  mac_level: info
  # ofh_level: info
  # fapi_level: info
  tracing_filename: gnb_trace.log

pcap:
  mac_enable: false                                               # Set to true to enable MAC-layer PCAPs.
  mac_filename: /tmp/gnb_mac.pcap                                 # Path where the MAC PCAP is stored.
  ngap_enable: true                                               # Set to true to enable NGAP PCAPs.
  ngap_filename: /tmp/gnb_ngap.pcap                               # Path where the NGAP PCAP is stored.

metrics:
  autostart_stdout_metrics: true
  rlc_json_enable: false                                          # Optional BOOLEAN (0). Enable RLC JSON metrics reporting. Supported: [0, 1].
  rlc_report_period: 1000
  pdcp_report_period: 1000
  enable_json_metrics: true                                       # Enable reporting metrics in JSON format
  addr: 127.0.0.1                                                 # Metrics-server IP
  port: 55555                                                     # Metrics-server Port
